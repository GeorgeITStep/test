﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int b = 0;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Enter = exit");
                Console.Write("Введите число a ");
                var x = Console.ReadLine();
                if (x != "")
                {
                    a = Convert.ToInt32(x);
                }
                else
                    break;
                Console.Write("Введите число b ");
                x = Console.ReadLine();
                if (x != "")
                {
                    b = Convert.ToInt32(x);
                }
                else
                    break;


                int sum = a + b;
                int res = a - b;
                int mul = a * b;
                Console.WriteLine($"{a} + {b} = {sum}");
                Console.WriteLine($"{a} - {b} = {res}");
                Console.WriteLine($"{a} * {b} = {mul}");
                if (b != 0)
                {
                    var div = a / b;
                    Console.WriteLine($"{a} / {b} = {div}");
                }
            }
            Console.WriteLine("exit...");
            Thread.Sleep(1500);
        }
    }
}
